home=$(pwd)

cd statAnalyser/edu/hkust/leap
javac StatAnalyser.java Util.java Parameters.java 
echo Compiled StatAnalyser 
cd $home 

cd ./statAnalyser
java edu.hkust.leap.StatAnalyser

cd $home
cd tmp
for file in *.trace.gz 
do
    mv "$file" "${file#*_}"
done

cd $home

# Replace values in ReplayDriver with stats
sedString1="sed -i '' '/accessVector/s/.*/TraceReader.readTrace(1,\""
sedString2="\/tmp\/accessVector.trace.gz\");/' src/replaydriver/ReplayDriver.java"
path=$(echo $(pwd) | sed 's,/,\\/,g')
sedString=$sedString1$path$sedString2
eval $sedString

sedString1="sed -i '' '/threadNameToIdMap/s/.*/TraceReader.readTrace(2,\""
sedString2="\/tmp\/threadNameToIdMap.trace.gz\");/' src/replaydriver/ReplayDriver.java"
path=$(echo $(pwd) | sed 's,/,\\/,g')
sedString=$sedString1$path$sedString2
eval $sedString

sedString1="sed -i '' '/nanoTimeDataVec/s/.*/TraceReader.readTrace(3,\""
sedString2="\/tmp\/nanoTimeDataVec.trace.gz\");/' src/replaydriver/ReplayDriver.java"
path=$(echo $(pwd) | sed 's,/,\\/,g')
sedString=$sedString1$path$sedString2
eval $sedString

sedString1="sed -i '' '/nanoTimeThreadVec/s/.*/TraceReader.readTrace(4,\""
sedString2="\/tmp\/nanoTimeThreadVec.trace.gz\");/' src/replaydriver/ReplayDriver.java"
path=$(echo $(pwd) | sed 's,/,\\/,g')
sedString=$sedString1$path$sedString2
eval $sedString

sed -i '' 's/{}/{'$1'}/' src/replaydriver/ReplayDriver.java
