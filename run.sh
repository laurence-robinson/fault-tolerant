#!/bin/bash
home=$(pwd)
ip=$(docker-machine ip)

# Clean up previous runs
./clean.sh

#Number of recordable versions to be created
echo Enter number of recordable versions needed:
read recNo

#Name of the test class package 
echo Enter package name of the test files
read package

#Compile test case
cd ./test
javac ./$package/$1.java
cd $home
echo Test files have been compiled. Press Enter to Continue

# Remove any current docker containers
docker rm -f $(docker ps -q) &>/dev/null

# Build and run local SSH server
cd SSH
echo Building SSH Image
docker build -t ssh . &>/dev/null
cd ..
echo Running SSH image
docker run -d -v $(pwd)/tmp:/tmp -p 4000:22 ssh 

# Build Docker image
echo Building Leap Image
docker build -t leap . &>/dev/null


#For the number of recordable versions wanted,
#run the transformer recNo of times while keeping
#the replayer version of the program intact and
#moving each recorder version into its own directory 
#along with a file containing details of its recorded SPEs
echo Generating containers to create recorders
counter=1
while [ $counter -le $recNo ]
do	
docker run -d leap ./docker-run-generate.sh $package $1 $counter $ip &>/dev/null
((counter++))
done

printf "Waiting for containers to finish"

while [ -n "$( docker ps | grep leap | awk '{print $1;}' )" ] 
do
    printf "."
    sleep 5
done 
printf "\n"

echo Done! Generating updated Leap image
docker build -t leap . &>/dev/null

#Another while loop that runs the recorder versions 
#creating partial logs. The read command is to obtain the 
#number of SPEs that is recorded by a particular record version
#which is needed as parameter for execution
echo Generating Replayers from containers
counter=1
while [ $counter -le $recNo ]
do	
docker run -d leap ./docker-run-replay.sh $1 $package $counter $ip &>/dev/null
((counter++))
done

printf "Waiting for containers to finish"

while [ -n "$( docker ps | grep leap | awk '{print $1;}' )" ] 
do
    printf "."
    sleep 1
done 
printf "\n"

echo Finished Recorder Stage
echo Retrieving Replay Driver
mkdir -p src/replaydriver
mv tmp/ReplayDriver.java src/replaydriver/

echo Colating Partial Logs
comma=","
quote='"'
# Grab arguments
for i in ${@:2}
do
    args=$args$comma$quote$i$quote
done
./stat.sh ${args:1}

echo Done ! To run replay please run ./replay.sh
