package edu.hkust.leap.replayer;

import java.util.Vector;
import edu.hkust.leap.tracer.TraceReader;

public class ReplayControl {
	
	public static void check(int index) {
		
		if(TraceReader.accessVector!=null)
		{
			String threadName = Thread.currentThread().getName();
			long threadId = TraceReader.threadNameToIdMap.get(threadName);
			if(threadId==1)
				return;
			long id;
			//index=0;
			
			Vector<Long> v = TraceReader.accessVector[index];
			
			synchronized (ActiveChecker.lock)
			{
				if(v.isEmpty())
					return;
				
				id = v.get(0);
				while(id==1)
				{
					v.remove(0);
					id = v.get(0);
				}
				
				if(threadId!=id)
				{
					(new ActiveChecker(index)).check();
				}
				
			}
			id = v.get(0);
			while(threadId!=id)
			{
				ActiveChecker.blockIfRequired();
				id = v.get(0);
			}
				
			v.remove(0);
			//System.out.println(index+" "+threadId);
		}
		else
		{
			
		}
			
	}	
	public static long getRandomSeed()
	{
		String threadName = Thread.currentThread().getName();
		long threadId = TraceReader.threadNameToIdMap.get(threadName);
		long id;
		
		Vector<Long> v = TraceReader.nanoTimeThreadVec;
		
		synchronized (ActiveChecker.lock)
		{

			id = v.get(0);
//			if(id==0)
//				return;
			
			if(threadId!=id)
			{
				(new ActiveChecker(0)).check();
			}
			
		}
		id = v.get(0);
		while(threadId!=id)
		{
			ActiveChecker.blockIfRequired();
			id = v.get(0);
		}
			
		v.remove(0);
		return TraceReader.nanoTimeDataVec.remove(0);
		
	}
}
