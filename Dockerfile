FROM centos
WORKDIR /home
RUN yum install -y wget openssh-clients
RUN wget --quiet --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.rpm"
RUN yum localinstall -y jdk-7u79-linux-x64.rpm

COPY SSH/docker /home/docker
RUN chmod 400 /home/docker

COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
