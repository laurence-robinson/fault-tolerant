# Leap with Docker #
This is the project folder for running Group 8's implementation of COOPREP using Docker. The benefits of this are parallelisation on a local machine as well as the capability to use Docker Swarm to spread the load over multiple computers. This is fully working on the local machine but more of a proof of concept for Swarm, it'll work but it's not production ready.

## Requirements ##
Here are the list of requirements to run COOPREP using Docker.

 1. [Docker](https://docs.docker.com/engine/installation/)

 2. [Docker-machine](https://docs.docker.com/machine/install-machine/)

 3. [Java 7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)

 4. Common bash tools - sed / printf / ssh / awk

## Checks ##
To ensure everything is running properly here are several checks you can use.

### Docker ###
If the following command works then Docker is set up correctly
    
    > docker run hello-world

### Docker Machine ###
If the following command works then Docker-Machine is set up correctly

    > docker-machine ip

## Instructions ##
Follow these instructions to run COOPREP

 1. Place your java source code into a folder in Test. e.g. Test/$projectName
 2. Run the following commands

    > ./clean.sh # Removes any leftovers from previous runs

    > ./run.sh Main args  # Runs COOPREP. The first argument is the entry point for the test program followed by any arguments required.

 3. You will be presented with the option of how many recorders to generate. The higher the number the more accurate the result but the longer it takes. You will then be asked for the program name which should be the same as you've named your folder in Test.

 4. Wait while COOPREP runs, can take a while
 5. Once complete run
    
    > ./replay.sh

    to replay the error

### Run using the given example ###
To run COOPREP using the given example run the following set of commands

    > ./clean.sh
    > ./run.sh Main 4 3
    > (Any number of recordable versions)
    > xyz

Once the program has finished
    
    > ./replay.sh

## A Note on Swarm and deploying across multiple computers ##
Docker Swarm is a new addition to Docker that lets you run containers across multiple hosts as if they were your own PC. Swarm should work out of the box. Follow the guidelines [here](https://docs.docker.com/engine/swarm/) to set up your swarm. The only requirements are to ensure that your computer is accepting incoming connections on the port 4000. You will also have to edit the 3rd line of run.sh and set $ip to your publically addressable IP.

## Troubleshooting ##
For all errors the first thing to try is the cleanup script manually

    > ./clean.sh

### fatal error: unexpected signal during runtime execution ###
The most common error received during testing that is a bug with Docker. Having multiple containers write to a shared space is something Docker supports and encourages but sometimes it causes errors, most commonly this one. To fix try the following steps.

First remove all running containers and start again. The command to remove all containers is as follows

    > docker rm -f $(docker ps -a -q)

If that doesn't work then you'll have to reboot the Docker virtual machine. 

    > docker-machine restart default

    > eval $(docker-machine env)

As well as clearing containers, make sure you run the clean script as well.
