package edu.hkust.leap;

import java.io.File;
import java.io.FilenameFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Vector;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;
import java.util.Iterator;
import edu.hkust.leap.Util;

public class StatAnalyser {
	public static Vector<Long>[] accessVectorArray = null;
	public static Vector<Long>[] accessVector2Array = null;
	public static Vector<Long> accessVector = null;
	public static Vector<Long> accessVector2 = null;
	public static double sameCounter = 0;
	public static double diffCounter = 0;
	//public static Vector<Long>[]<Long> totalVector<Long>[]s = null;
	public static double totalSPE = 0;
	public static double similarity = 0.0;
	
	public static void main(String[] args) {
		String filepath = System.getProperty("user.dir") + "/../tmp";
        File dir = new File(filepath);
        List<File> list = Arrays.asList(dir.listFiles(new FilenameFilter(){
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith("accessVector.trace.gz"); 
            }
        }));

        for (File element : list) 
            System.out.println(element.getPath());
		
    	try {
            List<ObjectInputStream> in = new ArrayList<ObjectInputStream>();
            List<Vector<Long>[]> accessVectors = new ArrayList<Vector<Long>[]>();

            for (File element : list) 
                in.add(new ObjectInputStream(new GZIPInputStream(new FileInputStream(element.getPath()))));
            
            for (ObjectInputStream input : in) {
                Vector<Long>[] vector = (Vector<Long>[]) Util.loadObject(input);
                accessVectors.add(vector);
            }

            for (Vector<Long>[] av : accessVectors) 
                for(int index=0; index < av.length; index++) System.out.println(av[index]);
            
                
			accessVectorArray = accessVectors.get(0);
			accessVector2Array = accessVectors.get(1);
        
            similar();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
	}
	
	public static void similar() {
		
		//if(accessVector[]Array.length == 1 && accessVector[]2Array.length == 1){
			System.out.print("Checking similarity");
			

			int speUpperBound = 0;
			int speLowerBound = 0;

			//Find outerloop bound and inner loop bound
			if (accessVectorArray.length > accessVector2Array.length) {
				speUpperBound = accessVectorArray.length;
				speLowerBound = accessVector2Array.length;
			} else {
				speUpperBound = accessVector2Array.length;
				speLowerBound = accessVectorArray.length;
			}

			for (int speCount = 0; speCount < speUpperBound; speCount++) {

				for (int speCount2 =0; speCount2<speLowerBound; speCount2++) {
					
					//Larger vector array will be the outerloop
					if (accessVectorArray.length > accessVector2Array.length) {
						accessVector = accessVectorArray[speCount];
						accessVector2 = accessVector2Array[speCount2];
					} else {
						accessVector = accessVectorArray[speCount2];
						accessVector2 = accessVector2Array[speCount];
					}

					//Cases when SPE recorded no threads
					if (accessVector.size()==0 && accessVector2.size()==0) {
						sameCounter++;
						continue;
					} else if(accessVector.size()==0 || accessVector2.size()==0) {
						diffCounter--;
						break;
					} 
					//Only do the check between the two logs if their size are equal otherwise, trivially determine 
					//they are dissimilar
					else if (accessVector.size() == accessVector2.size()) {
						//boolean to check if there has been any mismatch between thread entry in one SPE compared
						//to the other
						boolean correctSoFar = true;
						for(int threadCount = 0; threadCount < accessVector.size(); threadCount++) {
								//System.out.println(accessVector[].get(threadCount));
											//System.out.println(accessVector[]2.get(threadCount));
								if(accessVector.get(threadCount).equals(accessVector2.get(threadCount))) { 
									//System.out.println("Incrementing");
									continue;
								}	
								else {
									correctSoFar=false;
									break;
									//totalVector<Long>[]s.add(accessVector2.get(threadCount));
								}
						}
						if(correctSoFar) {
							sameCounter++;
							break;
						} else {
							diffCounter++;
						}

					} else {
						diffCounter++;
					}
				}
				}
			//Total number of SPEs amongst the two logs will be size of the greater log	
			totalSPE = speUpperBound;
			System.out.println(sameCounter);
			System.out.println(diffCounter);
			//Similarity metric
			similarity = (sameCounter/totalSPE)
								*(1-(diffCounter/totalSPE));
			
		
		//else {
			
		//	System.out.println("Access vector array length is not 1");
			
		//}
		
		System.out.println(similarity);
		
	}
	
}
