#!/bin/bash
java -cp .:./transformer/lib/jasminclasses-2.4.0.jar:\
./transformer/lib/polyglotclasses-1.3.5.jar:\
./transformer/lib/sootclasses-2.4.0.jar:\
./test:\
./transformer/bin edu.hkust.leap.Main $1.$2
cd /usr/src/myapp/tmp/runtime 
mkdir $3
mv $1 ./$3
cd .. 
mv $1.$2 ./runtime/$3
cd /usr/src/myapp
ssh -o StrictHostKeyChecking=no root@$4 -p 4000 -i "/home/docker" "mkdir -p /tmp/runtime"
scp -o StrictHostKeyChecking=no -P 4000 -i "/home/docker" -r tmp/runtime/$3 root@$4:/tmp/runtime
scp -o StrictHostKeyChecking=no -P 4000 -i "/home/docker" -r tmp/replay root@$4:/tmp
