#!/bin/bash
cd tmp/runtime/$3
read -r firstline<$2.$1
short=${firstline:0:1}
echo $short
cd /usr/src/myapp
java -cp ./tmp/runtime/$3:./recorder/bin edu.hkust.leap.Main $short $2.$1 
scp -o StrictHostKeyChecking=no -P 4000 -i "/home/docker" src/replaydriver/ReplayDriver.java root@$4:/tmp
scp -o StrictHostKeyChecking=no -P 4000 -i "/home/docker" tmp/*.trace.gz root@$4:/tmp
